#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.internal;

import org.osgi.service.component.ComponentContext;

import ${package}.${serviceName};

public class ${serviceName}Component implements ${serviceName} {

	private ${serviceName}Impl service; 
	
	public void activate(final ComponentContext context) {

	}

	public void deactivate() {

	}

    protected <T> T getService(final ComponentContext context, final Class<T> serviceInterface) throws IllegalStateException {
	@SuppressWarnings("unchecked")
	final T service = (T) context.locateService(serviceInterface.getSimpleName());
	if (null == service) {
	    throw new IllegalStateException(String.format("Please check the component definition. Service '%s' (type %s) could not be located!",
		    serviceInterface.getSimpleName(), serviceInterface.getName()));
	}
	return service;
    }
}
