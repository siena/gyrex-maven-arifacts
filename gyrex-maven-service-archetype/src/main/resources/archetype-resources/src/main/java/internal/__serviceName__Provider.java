#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.internal;

import ${package}.${serviceName};

public interface ${serviceName}Provider {

	${serviceName} getService();
}
